﻿namespace model
{

	/// <summary>
	/// Generator used to create Waves and elaborate local parameters and values.
	/// </summary>
	public interface WaveInfoGenerator
	{

		WaveInfo loadStageStandard(int stageNUM);

		WaveInfo_Difficulty Difficulty {get;}

	}

}